
/*
 * @Date         : 2021-02-16 22:30:23
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-16 22:30:25
 * @FilePath     : \QianjiAutoRecord\resources\defaultConfig\DefaultAccount.js
 */

var DefaultAccount = [
    {"from_account": "余额宝", "to_account": "支付宝"},
    {"from_account": "账户余额", "to_account": "支付宝"},
    {"from_account": "零钱", "to_account": "微信"},
    {"from_account": "工商银行储蓄卡(5555)", "to_account": "工商银行储蓄卡(5555)"},
    {"from_account": "中国工商银行储蓄卡(5555)", "to_account": "工商银行储蓄卡(5555)"}
]

module.exports = DefaultAccount