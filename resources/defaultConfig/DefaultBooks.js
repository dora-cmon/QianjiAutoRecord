
/*
 * @Date         : 2021-02-16 22:24:19
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-16 22:25:11
 * @FilePath     : \QianjiAutoRecord\resources\defaultConfig\DefaultBooks.js
 */

var DefaultBooks = [
    {
        "book_name": "日常账本",
        "enable": true,
        "支出": ["三餐", "零食", "水果", "衣服", "交通", "旅行", "孩子", "宠物", "话费网费", "烟酒", "学习", "日用品",
                 "住房", "美妆", "医疗", "发红包", "汽车/加油", "娱乐", "请客送礼", "电器数码", "运动", "其他"],
        "收入": ["工资", "生活费", "收红包", "外快", "股票基金", "其他"]
    },
    {
        "book_name": "旅行账本",
        "enable": true,
        "支出": ["吃喝", "交通", "门票", "住宿", "娱乐", "旅游装备", "其他"],
        "收入": ["红包", "其他"]
    },
    {
        "book_name": "装修账本",
        "enable": true,
        "支出": ["地板瓷砖", "门窗", "线路改造", "辅助材料", "厨房", "卫浴", "家具", "家电", "插座", "灯具",
                 "窗帘", "人工费", "设计费", "装饰物品", "油漆涂料", "其他"],
        "收入": ["红包", "其他"]
    },
    {
        "book_name": "结婚账本",
        "enable": true,
        "支出": ["聘礼", "首饰", "婚纱摄影", "酒席", "喜帖喜糖", "婚庆司仪", "度蜜月", "其他"],
        "收入": ["红包", "礼金", "其他"]
    },
    {
        "book_name": "人情账本",
        "enable": true,
        "支出": ["发红包", "婚嫁随礼", "寿辰", "乔迁", "其他"],
        "收入": ["收红包", "结婚收礼", "寿辰收礼", "乔迁收礼", "其他"]
    },
    {
        "book_name": "出差账本",
        "enable": true,
        "支出": ["交通", "酒店住宿", "餐饮", "宴请招待", "送礼", "其他"],
        "收入": ["差旅津贴", "红包", "其他"]
    },
    {
        "book_name": "公司账本",
        "enable": true,
        "支出": ["员工工资", "五险一金", "员工奖金", "员工团建", "水费电费", "网络通讯", "办公用品",
                 "场地租金", "维修费", "清洁费", "材料费", "物流费", "花卉", "其他"],
        "收入": ["公司营收", "租赁所得", "政策补贴", "退税收入", "物品转卖", "其他"],
    },
    {
        "book_name": "店铺生意",
        "enable": true,
        "支出": ["员工工资", "进货", "其他材料", "水费电费", "场地租金", "维修费", "清洁费", "物流费", "其他"],
        "收入": ["店铺收入", "物品转卖", "其他"]
    },
    {
        "book_name": "汽车账本",
        "enable": true,
        "支出": ["油费", "停车费", "洗车", "过路费", "罚款", "维修保养", "车贷", "配件", "车检", "车险", "购车款", "其他"],
        "收入": ["其他"]
    }
]

module.exports = DefaultBooks