
/*
 * @Date         : 2021-01-27 22:15:33
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-16 22:44:29
 * @FilePath     : \QianjiAutoRecord\resources\PageDict\WechatPageDict.js
 */

var WechatPageDict = {
    "微信": {
        "default_page": true,
        "package_name": "com.tencent.mm"
    },
    "微信发红包": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.plugin.luckymoney.ui.LuckyMoneyPrepareUI",
    },
    "微信收红包": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.plugin.luckymoney.ui.LuckyMoneyNotHookReceiveUI"
    },
    "微信红包详情": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.plugin.luckymoney.ui.LuckyMoneyDetailUI"
    },
    "微信发转账": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.plugin.remittance.ui.RemittanceUI",
        "text_list": ["转账", "转账金额"]
    },
    "微信添加转账说明": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.ui.widget.a.d"
    },
    "微信支付成功": {
        "package_name": "com.tencent.mm",
        "activity": "android.widget.LinearLayout",
        "text_list": ["支付成功", "完成"]
    },
    "微信支付成功1": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.plugin.remittance.ui.RemittanceBusiResultUI",
        "text_list": ["支付成功", "完成"]
    },
    "微信支付成功2": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.framework.app.UIPageFragmentActivity",
        "text_list": ["支付成功", "返回商家"]
    },
    "微信支付成功3": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.ui.base.i",
        "text_list": ["支付成功", "完成"]
    },
    "微信支付成功4": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.plugin.wallet_core.ui.WalletOrderInfoNewUI",
    },
    "微信支付成功5": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.framework.app.UIPageFragmentActivity",
        "text_list": ["支付成功", "完成"]
    },
    "微信收转账": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.plugin.remittance.ui.RemittanceDetailUI",
        "text_list": ["待你收款", "确定收款"]
    },
    "微信转账收取确认": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.ui.base.q",
        "text_list": ["你已收款", "收款时间"]
    },
    "微信转账收取确认1": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.plugin.remittance.ui.RemittanceDetailUI",
        "text_list": ["你已收款", "收款时间"]
    },
    "微信指纹支付": {
        "package_name": "com.tencent.mm",
        "activity": "android.widget.LinearLayout",
        "text_list": ["确认支付", "支付方式"]
    },
    "微信指纹支付1": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.framework.app.UIPageFragmentActivity",
        "text_list": ["确认支付", "支付方式"]
    },
    "微信密码支付": {
        "package_name": "com.tencent.mm",
        "activity": "android.widget.LinearLayout",
        "text_list": ["请输入支付密码", "支付方式"]
    },
    "微信密码支付1": {
        "package_name": "com.tencent.mm",
        "activity": "android.widget.LinearLayout",
        "text_list": ["请输入支付密码", "支付方式"]
    },
    "微信密码支付2": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.framework.app.UIPageFragmentActivity",
        "text_list": ["请输入支付密码", "支付方式"]
    },
    "微信验证指纹": {
        "package_name": "com.tencent.mm",
        "activity": "android.widget.FrameLayout",
        "text_list": ["请验证指纹"]
    },
    "微信验证指纹1": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.plugin.remittance.ui.RemittanceUI",
        "text_list": ["请验证指纹"]
    },
    "微信验证指纹2": {
        "package_name": "com.tencent.mm",
        "activity": "android.widget.LinearLayout",
        "text_list": ["请验证指纹"]
    },
    "微信支付加载": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.ui.base.i",
        "text_list": ["微信支付"]
    },
    "微信支付加载1": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.plugin.remittance.ui.RemittanceUI",
        "text_list": ["微信支付"]
    },
    "微信付款码": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.plugin.offline.ui.WalletOfflineCoinPurseUI"
    },
    "微信大付款码": {
        "package_name": "com.tencent.mm",
        "activity": "android.widget.FrameLayout",
        "id_list": ["com.tencent.mm:id/faa", "com.tencent.mm:id/fab", "com.tencent.mm:id/fa7"]
    },
    "微信大付款码1": {
        "package_name": "com.tencent.mm",
        "activity": "android.widget.FrameLayout",
        "id_list": ["com.tencent.mm:id/j3b", "com.tencent.mm:id/j3c"]
    },
    "微信选择付款方式": {
        "package_name": "com.tencent.mm",
        "activity": "android.support.design.widget.a",
        "text_list": ["选择优先的支付方式"]
    },
    "微信选择付款方式1": {
        "package_name": "com.tencent.mm",
        "activity": "android.widget.LinearLayout",
        "text_list": ["选择支付方式"]
    },
    "微信选择优惠": {
        "package_name": "com.tencent.mm",
        "activity": "android.widget.LinearLayout",
        "text_list": ["请选择优惠", "完成"]
    },
    "微信聊天界面": {
        "package_name": "com.tencent.mm",
        "activity": "android.widget.LinearLayout",
        "id_list": ["com.tencent.mm:id/ipt", "com.tencent.mm:id/iki"]
    },
    "微信聊天界面1": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.ui.LauncherUI",
        "id_list": ["com.tencent.mm:id/ipt", "com.tencent.mm:id/iki"]
    },
    "微信聊天界面2": {
        "package_name": "com.tencent.mm",
        "activity": "com.tencent.mm.ui.chatting.ChattingUI",
    }
}

module.exports = WechatPageDict