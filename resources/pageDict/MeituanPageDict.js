
/*
 * @Date         : 2021-02-14 12:06:51
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-18 18:22:50
 * @FilePath     : \QianjiAutoRecord\resources\pageDict\MeituanPageDict.js
 */

var MeituanPageDict = {
    "美团": {
        "default_page": true,
        "package_name": "com.sankuai.meituan"
    },
    "美团订单页": {
        "package_name": "com.sankuai.meituan",
        "activity": "com.meituan.android.cashier.activity.MTCashierActivity",
        "text_list": ["支付订单"],
    },
    "美团选择支付方式": {
        "package_name": "com.sankuai.meituan",
        "activity": "com.meituan.android.pay.common.selectdialog.view.b",
        "text_list": ["选择支付方式"],
    },
    "美团指纹支付": {
        "package_name": "com.sankuai.meituan",
        "activity": "com.meituan.android.pay.activity.PayActivity",
        "text_list": ["指纹支付"],
    },
    "美团指纹支付1": {
        "package_name": "com.sankuai.meituan",
        "activity": "com.meituan.android.paycommon.lib.fingerprint.VerifyFingerprintActivity",
        "text_list": ["指纹支付"],
    },
    "美团支付成功": {
        "package_name": "com.sankuai.meituan",
        "activity": "com.meituan.android.paybase.dialog.progressdialog.a",
        "text_list": ["支付成功"],
    },
    "美团支付成功1": {
        "package_name": "com.sankuai.meituan",
        "activity": "com.meituan.android.mrn.container.MRNBaseActivity",
        "text_list": ["购买成功!"],
    }
    
}

module.exports = MeituanPageDict