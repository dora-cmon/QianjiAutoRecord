
/*
 * @Date         : 2021-01-27 22:15:33
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-16 23:34:25
 * @FilePath     : \QianjiAutoRecord\resources\PageDict\AliPayPageDict.js
 */

var AliPayPageDict = {
    "支付宝": {
        "default_page": true,
        "package_name": "com.eg.android.AlipayGphone"
    },
    "支付宝付款码": {
        "package_name": "com.eg.android.AlipayGphone",
        "activity": "com.eg.android.AlipayGphone.AlipayLogin",
        "text_list": ["向商家付钱", "收钱", "扫码付"]
    },
    "支付宝付款码1": {
        "package_name": "com.eg.android.AlipayGphone",
        "activity": "com.alipay.mobile.onsitepay.merge.OnsitepayActivity",
        "text_list": ["向商家付钱", "收钱", "扫码付"]
    },
    "支付宝选择付款方式": {
        "package_name": "com.eg.android.AlipayGphone",
        "activity": "com.afollestad.materialdialogs.MaterialDialog",
        "text_list": ["选择优先付款方式"]
    },
    "支付宝选择付款方式1": {
        "package_name": "com.eg.android.AlipayGphone",
        "activity": "com.alipay.android.msp.ui.views.MspContainerActivity",
        "text_list": ["选择付款方式"]
    },
    "支付宝选择付款方式2": {
        "package_name": "com.eg.android.AlipayGphone",
        "activity": "android.app.Dialog",
        "text_list": ["选择付款方式"]
    },
    "支付宝发红包": {
        "package_name": "com.eg.android.AlipayGphone",
        "activity": "com.alipay.android.phone.discovery.envelope.universal.UniversalSendContainerActivity"
    },
    "支付宝发转账": {
        "package_name": "com.eg.android.AlipayGphone",
        "activity": "android.widget.FrameLayout",
        "text_list": ["转账金额", "转账记录"]
    },
    "支付宝支付成功": {
        "package_name": "com.eg.android.AlipayGphone",
        "activity": "com.alipay.android.msp.ui.views.MspContainerActivity",
        "text_list": ["支付成功", "付款方式"]
    },
    "支付宝支付成功1": {
        "package_name": "com.eg.android.AlipayGphone",
        "activity": "android.app.Dialog",
        "text_list": ["支付成功", "付款方式"]
    },
    "支付宝支付成功2": {
        "package_name": "com.eg.android.AlipayGphone",
        "activity": "com.alipay.android.msp.ui.views.MspUniRenderActivity",
        "text_list": ["支付成功", "付款方式"]
    },
    "支付宝转账成功": {
        "package_name": "com.eg.android.AlipayGphone",
        "activity": "com.alipay.android.msp.ui.views.MspContainerActivity",
        "text_list": ["转账成功", "收款方"]
    },
    "支付宝转账成功1": {
        "package_name": "com.eg.android.AlipayGphone",
        "activity": "android.app.Dialog",
        "text_list": ["转账成功", "收款方"]
    },
    "支付宝付款框": {
        "package_name": "com.eg.android.AlipayGphone",
        "activity": "com.alipay.android.msp.ui.views.MspContainerActivity",
        "text_list": ["订单信息", "付款方式"]
    },
    "支付宝指纹验证": {
        "package_name": "com.android.systemui",
        "activity": "android.widget.FrameLayout",
        "text_list": ["请验证指纹"]
    },
    "支付宝输入密码": {
        "package_name": "com.eg.android.AlipayGphone",
        "activity": "com.alipay.mobile.verifyidentity.module.password.pay.ui.PayPwdDialogActivity"
    },
    "支付宝聊天框": {
        "package_name": "com.eg.android.AlipayGphone",
        "activity": "com.alipay.mobile.chatapp.ui.PersonalChatMsgActivity_"
    }
}

module.exports = AliPayPageDict