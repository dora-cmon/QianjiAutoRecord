
/*
 * @Date         : 2021-02-17 19:15:32
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-18 19:52:17
 * @FilePath     : \QianjiAutoRecord\entrance.js
 */

auto.waitFor()

let storage = storages.create("钱迹自动记账数据库")
let setting_flag = storage.get("setting_flag", true)

if(setting_flag) alert("请点击右上角更多选项，禁用“音量上键关闭所有脚本”，启用“前台服务”。等待 5s 点击弹出框的确认按钮！")
while(setting_flag) {
    sleep(5000)
    setting_flag = !confirm("是否已经完成设置？\n（否 - 5s后继续弹出该框\n 是 - 进入自动记账程序）")
}
if(!setting_flag)   storage.put("setting_flag", false)

let excution = engines.execScriptFile("./main.js")

exit()