
/*
 * @Date         : 2021-02-18 16:10:22
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-18 18:16:52
 * @FilePath     : \QianjiAutoRecord\monitor\MeituanMonitor.js
 */

var MeituanMonitor = {}

MeituanMonitor.activePayMeituan = function (activity_monitor) {
    let related_page = ["美团订单页", "美团选择支付方式", "美团指纹支付", "美团支付成功"]
    let type = "支出"
    let money = 0.00
    let account = ""
    let description = ""
    let target = ""
    let remark = ""

    let timeCnt = activity_monitor.unrelatedPageTimeOutSecond / 100

    while(true){
        let curr_page = activity_monitor.pageRecord[activity_monitor.pageRecordIdx % activity_monitor.pageRecord.length]

        timeCnt = activity_monitor._preprocess(curr_page, related_page, timeCnt)
        if(timeCnt == 0)    break

        if(myUtil.inPageList(curr_page, ["美团订单页"])) {
            let _money = id("com.sankuai.meituan:id/business_info_money").findOnce()
            if(_money)  money = _money.text()

            let des = id("com.sankuai.meituan:id/order_name").findOnce()
            if(des)     description = des.text()
            
            let check_widgets = id("com.sankuai.meituan:id/ckb_cashier_pay_check").find()
            let account_widgets = id("com.sankuai.meituan:id/txt_cashier_pay_name").find()
            let _account = null
            for(var i=0; i<check_widgets.length; ++i) {
                if(check_widgets[i].checked() && i < account_widgets.length) {
                    _account = account_widgets[i]
                    break
                }
            }
            if(_account) account = _account.text()
        } else if(text("请输入支付密码").findOnce()) {
            timeCnt = activity_monitor.unrelatedPageTimeOutSecond / 100
            let account_pre = id("com.sankuai.meituan:id/bank_name").findOnce()
            let account_suf = id("com.sankuai.meituan:id/bank_name_ext").findOnce()

            if(account_pre) account = account_pre.text()
            if(account_suf) account = account + account_suf.text()
        } else if(myUtil.inPageList(curr_page, ["美团支付成功"])) {            // 更新商家
            let _money = textMatches(/((^[1-9]\d*)|^0)\.\d{2}$/).findOnce()
            if(_money)  money = _money.text()
            
            if(account && account.startsWith("余额"))   account = "余额"
            target = "美团商户"
            remark = "付款给" + target
            if(description) remark = remark + "，订单为：" + description
            
            activity_monitor._save(type, money, account, remark, target)
            break
        } else if(text("指纹支付").findOnce()) {
            timeCnt = activity_monitor.unrelatedPageTimeOutSecond / 100
        } else if(timeCnt < 70) {
            let preview_first = myUtil.findPreviewFirstInPageList(["美团支付成功", "美团订单页",], activity_monitor.pageRecord, activity_monitor.pageRecordIdx)
            // 支付成功
            if(!myUtil.inPageList(preview_first, ["美团支付成功"])) {     // 未支付切出 取消
                activity_monitor.debugInfo("未支付切出")
                activity_monitor._cancel()
                break
            }
        }

        if(!myUtil.inPageList(curr_page, ["美团订单页"]) && (account == "微信支付" || account == "银联手机闪付")) {
            toast("调用第三方支付")
            activity_monitor._cancel()
            break
        }
        sleep(100)
    }
}

module.exports = MeituanMonitor