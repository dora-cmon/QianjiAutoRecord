
/*
 * @Date         : 2021-02-16 23:08:20
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-17 13:42:40
 * @FilePath     : \QianjiAutoRecord\monitor\AliMonitor.js
 */

// const myUtil = require("../utils/Util.js")

var AliMonitor = {}

AliMonitor.activePayAli = function (activity_monitor) {
    let related_page = ["支付宝付款框", "支付宝指纹验证", "支付宝输入密码",
                        "支付宝支付成功", "支付宝转账成功", "支付宝选择付款方式"]
    let type = "支出"
    let money = 0.00
    let account = ""
    let description = ""
    let target = ""
    let remark = ""

    let timeCnt = activity_monitor.unrelatedPageTimeOutSecond / 100

    while(true){
        let curr_page = activity_monitor.pageRecord[activity_monitor.pageRecordIdx % activity_monitor.pageRecord.length]

        timeCnt = activity_monitor._preprocess(curr_page, related_page, timeCnt)
        if(timeCnt == 0)    break

        if(myUtil.inPageList(curr_page, ["支付宝付款框"])) {
            let _money = textMatches(/((^[1-9]\d*)|^0)\.\d{2}$/).findOnce()
            if(_money)  money = _money.text()
            let des = myUtil.widgetByParentChild(text("订单信息").findOnce(), 1, [1])
            if(des)     description = des.text()
            let _account = myUtil.widgetByParentChild(text("付款方式").findOnce(), 1, [2])
            if(_account) account = _account.text()
        } else if(myUtil.inPageList(curr_page, ["支付宝支付成功", "支付宝转账成功"])) {            // 更新商家
            let widget = myUtil.widgetByParentChild(text("收款方").findOnce(), 1, [1, 0])
            if(!widget) widget = myUtil.widgetByParentChild(text("付款方式").findOnce(), 3, [0, 0])
            if(widget)  target = widget.text()
            
            if(target) {
                remark = "向" + target
                if(myUtil.inPageList(curr_page, ["支付宝支付成功"]))         remark = remark + "付款"
                else if(myUtil.inPageList(curr_page, ["支付宝转账成功"]))    remark = remark + "转账"
            }
            
            if(!myUtil.isEmpty(description)) {
                if(!myUtil.isEmpty(remark))         remark = remark + "，"
                if(myUtil.inPageList(curr_page, ["支付宝支付成功"]))         remark = remark + "订单为：" + description
                else if(myUtil.inPageList(curr_page, ["支付宝转账成功"]))    remark = remark + "留言为：" + description
            }    

            activity_monitor._save(type, money, account, remark, target)
            break
        } else if(timeCnt < 70) {
            let preview_first = myUtil.findPreviewFirstInPageList(["支付宝支付成功", "支付宝付款框",], activity_monitor.pageRecord, activity_monitor.pageRecordIdx)
            // 支付成功
            if(!myUtil.inPageList(preview_first, ["支付宝支付成功"])) {     // 未支付切出 取消
                activity_monitor.debugInfo("未支付切出")
                activity_monitor._cancel()
                break
            }
        }
        sleep(100)
    }
}

AliMonitor.passivePayAli = function (activity_monitor) {
    let related_page = ["支付宝付款码", "支付宝指纹验证", "支付宝输入密码", "支付宝支付成功"]
    let type = "支出"
    let money = 0.00
    let account = ""
    let description = ""
    let target = ""
    let remark = ""

    let timeCnt = activity_monitor.unrelatedPageTimeOutSecond / 100

    while(true){
        let curr_page = activity_monitor.pageRecord[activity_monitor.pageRecordIdx % activity_monitor.pageRecord.length]

        timeCnt = activity_monitor._preprocess(curr_page, related_page, timeCnt)
        if(timeCnt == 0)    break

        if(myUtil.inPageList(curr_page, ["支付宝付款码"])) {
            let _account = id("com.alipay.mobile.onsitepay:id/selected_channel").findOnce()
            if(_account) account = _account.text()
            
        } else if(myUtil.inPageList(curr_page, ["支付宝支付成功"])) {
            let _money = textMatches(/((^[1-9]\d*)|^0)\.\d{2}$/).findOnce()
            if(_money)  money = _money.text()
            let des = myUtil.widgetByParentChild(text("订单信息").findOnce(), 1, [1])
            if(des)     description = des.text()
            let _target = myUtil.widgetByParentChild(textStartsWith("￥").findOnce(), 1, [0])
            if(_target)  target = _target.text()
            
            if(target)                          remark = "向" + target + "付款"
            if(!myUtil.isEmpty(description)){
                if(!myUtil.isEmpty(remark))         remark = remark + "，"
                remark = remark + "订单为：" + description
            }
            activity_monitor._save(type, money, account, remark, target)
            break
        } else if(timeCnt < 70) {
            let preview_first = myUtil.findPreviewFirstInPageList(["支付宝支付成功", "支付宝付款码",], activity_monitor.pageRecord, activity_monitor.pageRecordIdx)
            // 支付成功
            if(!myUtil.inPageList(preview_first, ["支付宝支付成功"])) {     // 未支付切出 取消
                activity_monitor.debugInfo("未支付切出")
                activity_monitor._cancel()
                break
            }
        }
        sleep(100)
    }
}

AliMonitor.taobaoPay = function (activity_monitor) {
    let related_page = ["淘宝付款框", "淘宝支付成功", "淘宝密码支付", "淘宝指纹支付", "淘宝选择付款方式", "支付宝输入密码"]
    let type = "支出"
    let money = 0.00
    let account = ""
    let description = ""
    let target = ""
    let remark = ""

    let timeCnt = activity_monitor.unrelatedPageTimeOutSecond / 100

    while(true){
        let curr_page = activity_monitor.pageRecord[activity_monitor.pageRecordIdx % activity_monitor.pageRecord.length]

        timeCnt = activity_monitor._preprocess(curr_page, related_page, timeCnt)
        if(timeCnt == 0)    break

        if(myUtil.inPageList(curr_page, ["淘宝付款框"])) {
            let _money = textMatches(/((^[1-9]\d*)|^0)\.\d{2}$/).findOnce()
            if(_money)  money = _money.text()
            let _account = myUtil.widgetByParentChild(text("付款方式").findOnce(), 1, [2])
            if(_account) account = _account.text()
        } else if(myUtil.inPageList(curr_page, ["淘宝支付成功"])) {
            target = "淘宝"
            remark = target + "付款"
            
            activity_monitor._save(type, money, account, remark, target)
            break
        } else if(text("请验证指纹").findOnce() || text("正在校验指纹...").findOnce()) {
                timeCnt = activity_monitor.unrelatedPageTimeOutSecond / 100
        } else if(timeCnt < 70) {
            let preview_first = myUtil.findPreviewFirstInPageList(["淘宝支付成功", "淘宝付款框",], activity_monitor.pageRecord, activity_monitor.pageRecordIdx)
            // 支付成功
            if(!myUtil.inPageList(preview_first, ["淘宝支付成功"])) {     // 未支付切出 取消
                activity_monitor.debugInfo("未支付切出")
                activity_monitor._cancel()
                break
            }
        }
        sleep(100)
    }
}

module.exports = AliMonitor