
/*
 * @Date         : 2021-02-17 12:39:37
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-18 21:10:37
 * @FilePath     : \QianjiAutoRecord\layout\MainPage.js
 */

let MainPage = function(auto_record) {
    ui.layout(
        <frame paddingtop="100px">
            <vertical>
                <text text="钱迹自动记账配置"  textSize="28sp" textColor="#000000" gravity="center_horizontal"/>

                <card w="*" h="auto" foreground="?selectableItemBackground" cardCornerRadius="32px" marginLeft="20px">
                    <vertical>
                        <text text="启用/禁用功能" textSize="18sp" textColor="#000000" gravity="center_horizontal"/>
                        <horizontal>
                            <checkbox id="wechat_box" text="微   信" layout_weight="1"/>
                            <checkbox id="alipay_box" text="支付宝" layout_weight="1"/>
                        </horizontal>
                        <horizontal>
                            <checkbox id="taobao_box" text="淘   宝" layout_weight="1"/>
                            <checkbox id="meituan_box" text="美   团" layout_weight="1"/>
                        </horizontal>
                    </vertical>
                </card>

                <card w="*" h="auto" foreground="?selectableItemBackground" cardCornerRadius="32px" marginLeft="20px">
                    <vertical>
                        <text text="全局变量配置" textSize="18sp" textColor="#000000" gravity="center_horizontal"/>
                        <horizontal>
                            <vertical layout_weight="9">
                                <horizontal>
                                    <text text="备注前缀："/>
                                    <input id="remark_prefix" textSize="14sp" hint="备注默认前缀" w="*"/>
                                </horizontal>
                                <horizontal>
                                    <text text="备注后缀："/>
                                    <input id="remark_suffix" textSize="14sp" hint="备注默认后缀" w="*"/>
                                </horizontal>
                                <checkbox id="last_remark_box" text="默认填写上次备注"/>
                                <horizontal>
                                    <checkbox id="has_account_box" text="资产账户" layout_gravity="1"/>
                                    <checkbox id="has_book_box" text="多账本" layout_gravity="1"/>
                                </horizontal>
                            </vertical>
                            <button id="global_config_ok" text="保存" w="150px" h="*" layout_weight="1"/>
                        </horizontal>
                    </vertical>
                </card>

                <card w="*" h="auto" foreground="?selectableItemBackground" cardCornerRadius="32px" marginLeft="20px">
                    <vertical>
                        <text text="相关设置" textSize="18sp" textColor="#000000" gravity="center_horizontal"/>
                        <horizontal>
                            <button id="account_config_btn" text="账户配置" layout_weight="1"/>
                            <button id="book_config_btn" text="账本配置" layout_weight="1"/>
                        </horizontal>
                        <button id="system_config" text="“音量上键关闭脚本”及“前台运行”"/>
                        <button id="history_manage" text="历史记账记录管理" layout_weight="1" visibility="gone"/>
                    </vertical>
                </card>
            </vertical>

            <frame gravity="bottom">
                <vertical gravity="bottom">
                    <button id="start_record" h="auto" text="开始自动记账" textSize="28sp"/>
                    <text text="Author: LiZhenglin | QQ: 10166266004" gravity="center|bottom"/>
                </vertical>
            </frame>
        </frame>
    )

    ui.remark_prefix.setText(auto_record.remark_prefix)
    ui.remark_suffix.setText(auto_record.remark_suffix)

    ui.has_book_box.attr("checked", auto_record.has_book)
    ui.has_account_box.attr("checked", auto_record.has_account)
    ui.last_remark_box.attr("checked", auto_record.last_remark)
    ui.wechat_box.attr("checked", auto_record.enable_feature["微信发红包"])
    ui.alipay_box.attr("checked", auto_record.enable_feature["支付宝付款码"])
    ui.taobao_box.attr("checked", auto_record.enable_feature["淘宝付款框"])
    ui.meituan_box.attr("checked", auto_record.enable_feature["美团订单页"])

    ui.account_config_btn.on("click", function () { auto_record.AccountConfigPage(auto_record)})
    ui.book_config_btn.on("click", function() { auto_record.BookConfigPage(auto_record) })
    ui.start_record.on("click", function() { auto_record.load(); auto_record.startRecord(); home() })
    ui.system_config.on("click", function() {
        auto_record.storage.put("setting_flag", true)
        for(var i=0; i<3; ++i) 
            toast("重新打开应用，进入系统设置页！");
        back()
    })

    // 保存启用特性
    ui.wechat_box.on("check", (checked) => {
        if(checked) {
            auto_record.enable_feature["微信发红包"] = true; 
            auto_record.enable_feature["微信收红包"] = true; 
            auto_record.enable_feature["微信发转账"] = true;
            auto_record.enable_feature["微信收转账"] = true;
            auto_record.enable_feature["微信指纹支付"] = true; 
            auto_record.enable_feature["微信密码支付"] = true;
            auto_record.enable_feature["微信付款码"] = true; 
            auto_record.saveEnableFeatureConfig() 
        } else {
            auto_record.enable_feature["微信发红包"] = false; 
            auto_record.enable_feature["微信收红包"] = false; 
            auto_record.enable_feature["微信发转账"] = false;
            auto_record.enable_feature["微信收转账"] = false;
            auto_record.enable_feature["微信指纹支付"] = false; 
            auto_record.enable_feature["微信密码支付"] = false;
            auto_record.enable_feature["微信付款码"] = false; 
            auto_record.saveEnableFeatureConfig() 
        }
    })
    ui.alipay_box.on("check", (checked) => {
        if(checked) {
            auto_record.enable_feature["支付宝付款框"] = true;
            auto_record.enable_feature["支付宝付款码"] = true;
            auto_record.saveEnableFeatureConfig()
        } else {
            auto_record.enable_feature["支付宝付款框"] = false;
            auto_record.enable_feature["支付宝付款码"] = false;
            auto_record.saveEnableFeatureConfig()
        }
    })
    ui.taobao_box.on("check", (checked) => {
        if(checked) auto_record.enable_feature["淘宝付款框"] = true;
        else        auto_record.enable_feature["淘宝付款框"] = false;
        auto_record.saveEnableFeatureConfig()
    })
    ui.meituan_box.on("check", (checked) => {
        if(checked) auto_record.enable_feature["美团订单页"] = true;
        else        auto_record.enable_feature["美团订单页"] = false;
        auto_record.saveEnableFeatureConfig()
    })

    // 保存修改备注配置
    ui.global_config_ok.on("click", function () {
        auto_record.remark_prefix = ui.remark_prefix.getText()
        auto_record.remark_suffix = ui.remark_suffix.getText()
        auto_record.saveGlobalConfig()
        alert("成功！")
    })
    ui.last_remark_box.on("check", function(checked) { auto_record.last_remark = checked; auto_record.saveGlobalConfig() })
    ui.has_account_box.on("check", function(checked) { auto_record.has_account = checked; auto_record.saveGlobalConfig() })
    ui.has_book_box.on("check", function(checked) {auto_record.has_book = checked; auto_record.saveGlobalConfig() })
}

module.exports = MainPage