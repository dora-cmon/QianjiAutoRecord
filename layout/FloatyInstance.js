
/*
 * @Date         : 2021-01-23 23:57:21
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-14 11:57:16
 * @FilePath     : \QianjiAutoRecord\layout\FloatyInstance.js
 */

/*
 * @Date         : 2021-01-23 23:43:08
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-01-24 16:26:53
 * @FilePath     : \钱迹自动记账\layout\FloatyInstance.js
 */

// const myUtil = require("../utils/Util.js")

let _debugInfo = typeof debugInfo === 'undefined' ? (v) => console.verbose(v) : debugInfo
let _errorInfo = typeof errorInfo === 'undefined' ? (v) => console.error(v) : errorInfo

let FloatyInstance = function () {
    this.floatyWindow = null
    this.floatyInitStatus = false
    this.floatyLock = null
    this.floatyCondition = null

    this.showLog = true
    this.debugInfo = function (content) {
      this.showLog && _debugInfo(content)
    }
}

FloatyInstance.prototype.getResult = function () {
    while(!this.confirm_flag || !this.cancel_flag) {
    }
    
    if(this.confirm_flag)  return {
        "type": this.type,
        "money": this.money,
        "account": this.account,
        "category": this.category,
        "book": this.book,
        "remark": this.remark,
        "target": this.target
    }
    if(this.cancel_flag)   return null
}

FloatyInstance.prototype.init = function (analyze_param, config_param, global_config) {
    if (this.floatyInitStatus) {
        return true
    }

    this.type = "支出"
    this.money = 0.00
    this.account = ""
    this.category = ""
    this.book = ""
    this.target = analyze_param["target"]

    this.second = 10
    this.data_type = ""
    this.data_list = []
    this.data_page = 0
    this.selected_data = ""

    this.confirm_flag = false
    this.cancel_flag = false

    this.analyze_param = analyze_param
    this.config_param = config_param
    this.global_config = global_config
    // this.history_remark_list = storage.get("_history_remark_")
    this.storage = storages.create("钱迹自动记账数据库")
    this.history_data_list = this.storage.get(this.target)
    this.book_cate_list_out = []
    this.book_cate_list_in = []
    
    this.remark_list_in = []
    this.remark_list_out = []
    if(!myUtil.isEmpty(this.history_data_list)) {
        for(var i=0; i<this.history_data_list.length; ++i) {
            let old_book_cate = this.history_data_list[i]["book"] + " || " + this.history_data_list[i]["category"]
            if(this.history_data_list[i]["type"] == "支出" && !myUtil.inList(old_book_cate, this.book_cate_list_out)) {
                this.book_cate_list_out.push(old_book_cate)          
            } else if(this.history_data_list[i]["type"] == "收入" && !myUtil.inList(old_book_cate, this.book_cate_list_in)) {
                this.book_cate_list_in.push(old_book_cate)
            }
                
        }
        this.book_cate_list_out = myUtil.reverseList(this.book_cate_list_out)
        this.book_cate_list_in = myUtil.reverseList(this.book_cate_list_in)
        
        for(var i=0; i<this.history_data_list.length; ++i) {
            let old_remark = this.history_data_list[i]["remark"]
            if(this.history_data_list[i]["type"] == "支出" && !myUtil.inList(old_remark, this.remark_list_out)) {
                this.remark_list_out.push(old_remark)
            } else if(this.history_data_list[i]["type"] == "收入" && !myUtil.inList(old_remark, this.remark_list_in)) {
                this.remark_list_in.push(old_remark)
            }
        }
    }
    if(!myUtil.inList(analyze_param["remark"], this.remark_list_in)) {
        this.remark_list_in.push(analyze_param["remark"])
    } else {
        let idx = 0
        for(; idx<this.remark_list_in.length; ++idx) {
            if(analyze_param["remark"] == this.remark_list_in[idx])  break
        }
        this.remark_list_in = myUtil.toListEnd(idx, this.remark_list_in)
    }
    if(!myUtil.inList(analyze_param["remark"], this.remark_list_out)) {
        this.remark_list_out.push(analyze_param["remark"])
    } else {
        let idx = 0
        for(; idx<this.remark_list_out.length; ++idx) {
            if(analyze_param["remark"] == this.remark_list_out[idx])  break
        }
        this.remark_list_out = myUtil.toListEnd(idx, this.remark_list_out)
    }
        
    this.remark_list_in = myUtil.reverseList(this.remark_list_in)
    this.remark_list_out = myUtil.reverseList(this.remark_list_out)
    
    this.floatyLock = threads.lock()
    this.floatyCondition = this.floatyLock.newCondition()
    let _this = this
    let W = device.width
    threads.start(function () {
        // 延迟初始化，避免死机
        sleep(200)
        _this.floatyLock.lock()
        try {
        if (_this.floatyInitStatus) {
            return true
        }
        _this.floatyWindow = floaty.rawWindow(
            <frame id="main_frame">
                <vertical>
                    <frame w="auto">
                        <card id="main_card" w="400px" marginLeft="32px" marginTop="32px" cardBackgroundColor="#ffffff" cardCornerRadius="32px" alpha="0.85">
                            <frame w="368px" marginLeft="32px">
                                <vertical>
                                    <frame w="*">
                                        <text id="money" text="0.00" textSize="24sp" gravity="center_horizontal" layout_gravity="center_horizontal"/>
                                    </frame>
                                    <horizontal>
                                        <text text="账户: " textColor="#000000"/>
                                        <frame>
                                            <card w="*" cardBackgroundColor="#000000" cardCornerRadius="16px" alpha="0.25"/>
                                            <text id="account" text="未定义账户" textColor="#000000"/>
                                        </frame>
                                    </horizontal>
                                    <text text="" textSize="2sp"/>
                                    <horizontal>
                                        <text text="类别: " textColor="#000000"/>
                                        <frame>
                                            <card w="*" cardBackgroundColor="#000000" cardCornerRadius="16px" alpha="0.25"/>
                                            <text id="category" text="其他" textColor="#000000"/>
                                        </frame>
                                    </horizontal>
                                    <text text="" textSize="2sp"/>
                                    <horizontal>
                                        <text text="账本: " textColor="#000000"/>
                                        <frame>
                                            <card w="*" cardBackgroundColor="#000000" cardCornerRadius="16px" alpha="0.25"/>
                                            <text id="book" text="未定义账本" textColor="#000000"/>
                                        </frame>
                                    </horizontal>
                                    <horizontal>
                                        <text text="备注: " textColor="#000000"/>
                                        <input id="remark" focusable="true" textSize="14sp" textColor="#000000"/>
                                    </horizontal>
                                    <text text="" textSize="20sp"/>
                                </vertical>
                            </frame>
                        </card>
                    </frame>
                    
                    <frame id="confirm_frame" w="400px" h="100px" marginTop="-40px" layout_gravity="center_horizontal">
                        <img src="#559AF9" w="120px" circle="true" layout_gravity="center"/>
                        <img id="confirm_btn" src="@drawable/ic_attach_money_black_48dp"
                            w="120px" h="120px" layout_gravity="center"/>
                    </frame>

                    <frame w="400px" id="data_frame" marginTop="-20px">
                        <vertical h="*" marginTop="65px">
                            <frame id="elem_frame_0" visibility="gone">
                                <card id="elem_card_0" w="*" cardBackgroundColor="#ffffff" cardCornerRadius="32px" foreground="?selectableItemBackgroud" alpha="0.85">
                                <text id="elem_data_0" textSize="14sp" marginLeft="15px"/>
                                </card>
                            </frame>
                            <text text="" textSize="2sp"/>
                            <frame id="elem_frame_1" visibility="gone">
                                <card id="elem_card_1" w="*" cardBackgroundColor="#ffffff" cardCornerRadius="32px" foreground="?selectableItemBackgroud" alpha="0.85">
                                <text id="elem_data_1" textSize="14sp" marginLeft="15px"/>
                                </card>
                            </frame>
                            <text text="" textSize="2sp"/>
                            <frame id="elem_frame_2" visibility="gone">
                                <card id="elem_card_2" w="*" cardBackgroundColor="#ffffff" cardCornerRadius="32px" foreground="?selectableItemBackgroud" alpha="0.85">
                                <text id="elem_data_2" textSize="14sp" marginLeft="15px"/>
                                </card>
                            </frame>
                            <text text="" textSize="2sp"/>
                            <frame id="elem_frame_3" visibility="gone">
                                <card id="elem_card_3" w="*" cardBackgroundColor="#ffffff" cardCornerRadius="32px" foreground="?selectableItemBackgroud" alpha="0.85">
                                <text id="elem_data_3" textSize="14sp" marginLeft="15px"/>
                                </card>
                            </frame>
                            <text text="" textSize="2sp"/>
                            <frame id="elem_frame_4" visibility="gone">
                                <card id="elem_card_4" w="*" cardBackgroundColor="#ffffff" cardCornerRadius="32px" foreground="?selectableItemBackgroud" alpha="0.85">
                                <text id="elem_data_4" textSize="14sp" marginLeft="15px"/>
                                </card>
                            </frame>
                            <text text="" textSize="2sp"/>
                            <frame id="elem_frame_5" visibility="gone">
                                <card id="elem_card_5" w="*" cardBackgroundColor="#ffffff" cardCornerRadius="32px" foreground="?selectableItemBackgroud" alpha="0.85">
                                <text id="elem_data_5" textSize="14sp" marginLeft="15px"/>
                                </card>
                            </frame>
                            <text text="" textSize="2sp"/>
                            <frame id="elem_frame_6" visibility="gone">
                                <card id="elem_card_6" w="*" cardBackgroundColor="#ffffff" cardCornerRadius="32px" foreground="?selectableItemBackgroud" alpha="0.85">
                                <text id="elem_data_6" textSize="14sp" marginLeft="15px"/>
                                </card>
                            </frame>
                            <text text="" textSize="2sp"/>
                            <frame id="elem_frame_7" visibility="gone">
                                <card id="elem_card_7" w="*" cardBackgroundColor="#ffffff" cardCornerRadius="32px" foreground="?selectableItemBackgroud" alpha="0.85">
                                <text id="elem_data_7" textSize="14sp" marginLeft="15px"/>
                                </card>
                            </frame>
                            <text text="" textSize="2sp"/>
                            <frame id="elem_frame_8" visibility="gone">
                                <card id="elem_card_8" w="*" cardBackgroundColor="#ffffff" cardCornerRadius="32px" foreground="?selectableItemBackgroud" alpha="0.85">
                                <text id="elem_data_8" textSize="14sp" marginLeft="15px"/>
                                </card>
                            </frame>
                            <text text="" textSize="2sp"/>
                            <frame id="elem_frame_9" visibility="gone">
                                <card id="elem_card_9" w="*" cardBackgroundColor="#ffffff" cardCornerRadius="32px" foreground="?selectableItemBackgroud" alpha="0.85">
                                <text id="elem_data_9" textSize="14sp" marginLeft="15px"/>
                                </card>
                            </frame>
                        </vertical>

                        <frame id="left_frame" w="auto" h="60px"  layout_gravity="left" alpha="0.85" visibility="invisible">
                            <img id="left_bg" src="#559AF9" w="60px" marginLeft="32px"/>
                            <img src="@drawable/ic_arrow_back_black_48dp" w="*" h="*"/>
                        </frame>

                        <frame id="right_frame" w="auto" h="60px" layout_gravity="right" alpha="0.85" visibility="invisible">
                            <img id="right_bg" src="#559AF9" w="60px" marginLeft="32px"/>
                            <img src="@drawable/ic_arrow_forward_black_48dp" w="*" h="*"/>
                        </frame>
                    </frame>
                </vertical>

                <frame id="close_frame" w="64px" h="64px">
                    <img id="left_top_circle" src="#FF0000" w="*" h="*" circle="true"/>
                    <img id="close_btn" src="@drawable/ic_close_black_48dp" w="*" h="*" marginLeft="1px" marginTop="3px" visibility="gone"/>
                    <text id="time"  textColor="#000000" textSize="20sp" marginLeft="1px" marginTop="-4px" gravity="center"/>
                </frame>
            </frame>
        )

        // 设置悬浮窗位置，金额，账户，备注，类别，账本，备注
        _this.floatyWindow.setPosition(W - 400, 100)

        _this.money = analyze_param["money"]
        if(!myUtil.isEmpty(_this.money)) _this.floatyWindow.money.setText(_this.money)

        if(!myUtil.isEmpty(_this.type)) _this.type = analyze_param["type"]
        if(_this.type == "支出") {
            _this.floatyWindow.money.setTextColor(colors.parseColor("#FF0000"))
        }else if(_this.type == "收入") {
            _this.floatyWindow.money.setTextColor(colors.parseColor("#00FF00"))
        }

        _this.account = analyze_param["account"]
        if(!_this.global_config["has_account"]) _this.account = "无账户"
        if(!myUtil.isEmpty(_this.account)) _this.floatyWindow.account.setText(_this.account)
        else                               _this.floatyWindow.account.setText("默认账户")

        // 历史数据不为空，自动填充上一次的数据
        if(!myUtil.isEmpty(_this.history_data_list)){
            let last_data = null
            for(var j=_this.history_data_list.length - 1; j>=0; --j) {
                if(_this.history_data_list[j]["type"] == _this.type) {
                    last_data = _this.history_data_list[j]
                    break
                }
            }
            if(!myUtil.isEmpty(last_data)) {
                if(!myUtil.isEmpty(last_data["category"]))   _this.category = last_data["category"]
                if(!myUtil.isEmpty(last_data["book"]))       _this.book = last_data["book"]
                if(_this.global_config["last_remark"] && !myUtil.isEmpty(last_data["remark"])) {
                    _this.remark = last_data["remark"]
                }
            }
        }
        
        if(!_this.global_config["last_remark"] && !myUtil.isEmpty(analyze_param["remark"]))
            _this.remark = analyze_param["remark"]
        if(!myUtil.isEmpty(_this.remark))   _this.floatyWindow.remark.setText(_this.remark)
        if(myUtil.isEmpty(_this.category)) _this.category = "其他"
        _this.floatyWindow.category.setText(_this.category)

        if(myUtil.isEmpty(_this.book) && config_param["books_list"].length > 0) _this.book = config_param["books_list"][0]
        // if(!_this.global_config["has_book"])    _this.book = "无账本"
        if(!myUtil.isEmpty(_this.book))   _this.floatyWindow.book.setText(_this.book)

        

        // 加载 类别|备注 数据列表
        _this.data_type = "book_cate"
        if(_this.type == "支出") {
            _this.data_list = _this.book_cate_list_out
        }else if(_this.type == "收入") {
            _this.data_list = _this.book_cate_list_in
        }
        _this.data_page = 0
        _this.selected_data = _this.book + " || " + _this.category
        _this.bindData()
        
        _this.floatyWindow.setTouchable(true)
        // 倒计时
        _this.floatyWindow.time.setText(_this.second-- + "")
        _this.timeCount = setInterval(function (){
            if(_this.second == -1) {
                clearInterval(_this.timeCount)
                _this.second--
            } else if(_this.second > 0) {
                ui.run( function() {_this.floatyWindow.time.setText(_this.second-- + "") })
            } else if(_this.second == 0) {
                clearInterval(_this.timeCount)
                _this.save()
            }
        }, 1000)
        
        // 保持开启
        setInterval( () => {}, 1000 )
        // 关闭按钮
        _this.floatyWindow.close_frame.on("click", () => { _this.close() })
        // 确认按钮
        _this.floatyWindow.confirm_frame.on("click", () => {
            _this.remark = _this.floatyWindow.remark.getText()
            _this.save()
        })

        // 设置事件响应
        _this.listenTouch()
        
        _this.floatyInitStatus = true
        } catch (e) {
        _errorInfo('悬浮窗初始化失败' + e)
        _this.floatyInitStatus = false
        } finally {
        _this.floatyCondition.signalAll()
        _this.floatyLock.unlock()
        }
    })
    this.floatyLock.lock()
    try {
        if (this.floatyInitStatus === false) {
        this.debugInfo('等待悬浮窗初始化')
        this.floatyCondition.await()
        }
    } finally {
        this.floatyLock.unlock()
    }
    this.debugInfo('悬浮窗初始化' + (this.floatyInitStatus ? '成功' : '失败'))
    return this.floatyInitStatus
}

FloatyInstance.prototype.listenTouch = function () {
    let _this = this
    ui.run(function () {
        // 填写备注
        _this.floatyWindow.remark.on("touch_down", () => {
            _this.debugInfo("填写备注")
            _this.stopCounting()
            _this.data_type = "remark"
            _this.floatyWindow.requestFocus();
            _this.floatyWindow.remark.requestFocus();
            if(_this.type == "支出")    _this.data_list = _this.remark_list_out
            else                        _this.data_list = _this.remark_list_in
            _this.data_page = 0
            _this.selected_data = _this.remark
            _this.bindData()
        });
        // 修改账户
        _this.floatyWindow.account.setOnTouchListener(function(view, event) {
            _this.debugInfo("选择账户")
            _this.stopCounting()
            _this.data_type = "account"
            _this.data_list = _this.config_param["accounts_list"]
            _this.data_page = 0
            _this.selected_data = _this.account
            _this.updateSelect()
            _this.bindData()
            return true
        })
        // 修改类别
        _this.floatyWindow.category.setOnTouchListener(function(view, event) {
            _this.debugInfo("选择类别")
            _this.stopCounting()
            _this.data_type = "category"
            _this.data_list = _this.config_param["books_dict"][_this.book][_this.type]
            _this.data_page = 0
            _this.selected_data = _this.category
            _this.updateSelect()
            _this.bindData()
            return true
        })
        // 修改账本
        _this.floatyWindow.book.setOnTouchListener(function(view, event) {
            _this.debugInfo("选择账本")
            _this.stopCounting()
            _this.data_type = "book"
            _this.data_list = _this.config_param["books_list"]
            _this.data_page = 0
            _this.selected_data = _this.book
            _this.updateSelect()
            _this.bindData()
            return true
        })
        // 切换数据列表
        let x = 0
        // 选择数据
        _this.floatyWindow.data_frame.setOnTouchListener(function (view, event) {
            _this.stopCounting()
            let action = event.getAction()
            if(action == event.ACTION_DOWN) {
                x = event.getRawX();
            } else if(action == event.ACTION_UP) {
                if(event.getRawX() > x + 20)      _this.data_page = _this.data_page > 0 ? _this.data_page - 1 : 0
                else if(event.getRawX() < x - 20) _this.data_page += 1
                _this.bindData()
            }
            return true
        })
        for(var i=0; i<10; ++i) {
            _this.floatyWindow["elem_data_" + i].setOnTouchListener(function (view, event) {
                _this.stopCounting()
                let action = event.getAction()
                if(action == event.ACTION_DOWN) {
                    x = event.getRawX();
                } else if(action == event.ACTION_UP) {
                    if(event.getRawX() > x + 20)      _this.data_page = _this.data_page > 0 ? _this.data_page - 1 : 0
                    else if(event.getRawX() < x - 20) _this.data_page += 1
                    else                              _this.selected_data = view.getText()

                    _this.bindData()
                }
                return true
            })
        }
        
        // 点击停止倒计时
        // _this.floatyWindow.main_card.setOnTouchListener(function (view, event) {
        //     _this.debugInfo("操作界面")
        //     _this.stopCounting()
        //     return true
        // })
    })
}

FloatyInstance.prototype.bindData = function() {
    let _this = this
    let total_page = 0
    
    if(!myUtil.isEmpty(_this.data_list))
        total_page = (_this.data_list.length - _this.data_list.length % 10) / 10
    if(_this.data_page > total_page)    _this.data_page = total_page
    
    ui.run(function () {
        if(myUtil.isEmpty(_this.data_list) || total_page <= 0) {
            _this.floatyWindow.left_frame.attr("visibility", "invisible")
            _this.floatyWindow.right_frame.attr("visibility", "invisible")
        } else {
            _this.floatyWindow.left_frame.attr("visibility", "visible")
            _this.floatyWindow.right_frame.attr("visibility", "visible")
            if(_this.data_page == 0) {
                _this.floatyWindow.left_bg.attr("src", "#ffffff")
                _this.floatyWindow.right_bg.attr("src", "#559AF9")
            } else if(_this.data_page == total_page) {
                _this.floatyWindow.left_bg.attr("src", "#559AF9")
                _this.floatyWindow.right_bg.attr("src", "#ffffff")
            } else {
                _this.floatyWindow.left_bg.attr("src", "#559AF9")
                _this.floatyWindow.right_bg.attr("src", "#559AF9")
            }
        }
        for(var i=0; i<10; ++i) {
            _this.floatyWindow["elem_frame_" + i].attr("visibility", "gone")
        }
        for(var i=_this.data_page * 10; i<_this.data_list.length && i<_this.data_page * 10 + 10; ++i) {
            _this.floatyWindow["elem_data_" + (i%10)].setText(_this.data_list[i])
            _this.floatyWindow["elem_frame_" + (i%10)].attr("visibility", "visible")
        }
        _this.updateSelect()
    })
}

FloatyInstance.prototype.updateSelect = function () {
    let _this = this
    ui.run(function () {
        try {
            for(var i=0; i<10; ++i) {
                let widget = _this.floatyWindow["elem_card_" + i]
                let data = _this.floatyWindow["elem_data_" + i].getText()
                if(_this.selected_data) {
                    if(data == _this.selected_data) {
                        widget.attr("cardBackgroundColor", "#559AF9")
                    } else {
                        widget.attr("cardBackgroundColor", "#ffffff")
                    }
                } else {
                    widget.attr("cardBackgroundColor", "#ffffff")
                }
            }
            
            if(_this.selected_data) {
                if(_this.data_type == "book"){
                    _this.book = _this.selected_data
                    _this.floatyWindow.book.setText(_this.book)
                } else if(_this.data_type == "account"){
                    _this.account = _this.selected_data
                    _this.floatyWindow.account.setText(_this.account)
                } else if(_this.data_type == "category"){
                    _this.category = _this.selected_data
                    _this.floatyWindow.category.setText(_this.category)
                } else if(_this.data_type == "remark") {
                    _this.remark = _this.selected_data
                    _this.floatyWindow.remark.setText(_this.remark)
                } else if(_this.data_type == "book_cate") {
                    let book_cate = _this.selected_data.split(" || ")
                    _this.book = book_cate[0]
                    _this.category = book_cate[1]
                    if(!myUtil.isEmpty(_this.book)) _this.floatyWindow.book.setText(_this.book)
                    _this.floatyWindow.category.setText(_this.category)
                }
            }
            
        } catch (e) {
            _errorInfo('更新选择失败' + e)
            _this.floatyInitStatus = false
        }
    })
}

FloatyInstance.prototype.stopCounting = function () {
    let _this = this
    ui.run( function() {
        _this.floatyWindow.time.attr("visibility", "gone")
        _this.floatyWindow.close_btn.attr("visibility", "visible")
    })
    _this.second = -1
}

FloatyInstance.prototype.close = function () {
    if (this.floatyInitStatus) {
        this.floatyLock.lock()
        try {
            if (this.floatyWindow !== null) {
                this.second = -1
                this.cancel_flag = true;
                this.floatyWindow.close()
                this.floatyWindow = null
            }
            this.floatyInitStatus = false
        } finally {
            this.floatyLock.unlock()
        }
    }
}

FloatyInstance.prototype.save = function () {
    if(!this.global_config["has_book"])    this.book = "无账本"
    this.confirm_flag = true;
    this.close()
}

module.exports = new FloatyInstance()