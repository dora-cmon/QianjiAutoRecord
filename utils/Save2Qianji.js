
/*
 * @Date         : 2021-01-10 11:52:23
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-10 18:52:31
 * @FilePath     : \QianjiAutoRecord\utils\Save2Qianji.js
 */

const { account } = require("../layout/FloatyInstance");

// const myUtil = require("./Util.js")

var Save2Qianji = {
    /**
     * @description: 调用钱迹接口记账
     * @param {Int} type: 账单类型，0（支出），1（收入），不填默认为支出
     * @param {Double} money: 账单金额，金额必须大于 0
     * @param {String} remark: 备注信息，默认为空
     * @param {String} catename: 账单分类，默认使用“其他”分类，若为“更多”，则弹出分类选择面板
     * @param {String} accountname: 账单所属资产名称，若无此资产会提示参数错误
     * @param {String} bookname: 账单所属账本名称，若无此资产会提示参数错误
     * @return: 无返回值
     */
    save : function (record_param) {
        let type = record_param["type"]
        let money = record_param["money"]
        let accountname = record_param["account"]
        let catename = record_param["category"]
        let remark = record_param["remark"]
        let bookname = record_param["book"]
        let _uri_prefix = "qianji://publicapi/addbill?"
        let out_str = ""
        
        // 构造 uri
        if(type == "支出")      { _uri = _uri_prefix + "&type=0"; out_str = out_str + "类型：支出\n" }
        else if(type == "收入") { _uri = _uri_prefix + "&type=1"; out_str = out_str + "类型：收入\n" }
        else   { alert("type 必须为收入或支出"); exit() }
        
        _uri = _uri + "&money=" + money; out_str = out_str + "金额：" + money + "\n"
        
        if(!myUtil.isEmpty(accountname) && accountname != "无账户") { _uri = _uri + "&accountname=" + accountname; out_str = out_str + "账户：" + accountname + "\n" }
        if(!myUtil.isEmpty(catename))    { _uri =_uri + "&catename=" + catename; out_str = out_str + "类别：" + catename + "\n" }
        if(!myUtil.isEmpty(bookname) && bookname != "无账本")    { _uri = _uri + "&bookname=" + bookname; out_str = out_str + "账本：" + bookname + "\n" }
        if(!myUtil.isEmpty(remark))      { _uri = _uri + "&remark=" + remark; out_str = out_str + "备注：" + remark }
        
        console.log(_uri)
        toast(out_str)
        // 调用钱迹接口
        app.startActivity({
            action: "VIEW",
            packageName: "com.mutangtech.qianji",
            data: _uri
        });
    }
};

module.exports = Save2Qianji;