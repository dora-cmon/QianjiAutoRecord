
/*
 * @Date         : 2021-01-10 11:28:37
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-18 19:30:17
 * @FilePath     : \QianjiAutoRecord\main.js
 */
"ui";

events.on("exit", function(){
    back()
});

const myUtil = require("./utils/Util.js")
const autoRecord = require("./AutoRecord.js");

autoRecord.run()